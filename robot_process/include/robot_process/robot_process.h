/*!*********************************************************************************
 *  \file       robot_process.h
 *  \brief      RobotProcess definition file.
 *  \details    This file contains the RobotProcess declaration. To obtain more information about
 *              it's definition consult the robot_process.cpp file.
 *  \authors    Jose Luis Sanchez-Lopez
 *
 *     This program is free software: you can redistribute it and/or modify 
 *     it under the terms of the GNU General Public License as published by 
 *     the Free Software Foundation, either version 3 of the License, or 
 *     (at your option) any later version. 
 *   
 *     This program is distributed in the hope that it will be useful, 
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 *     GNU General Public License for more details. 
 *   
 *     You should have received a copy of the GNU General Public License 
 *     along with this program. If not, see http://www.gnu.org/licenses/. 
 ********************************************************************************/

#ifndef _ROBOT_PROCESS_H_
#define _ROBOT_PROCESS_H_

#include <string>
#include <stdio.h>
#include <pthread.h>
#include <ros/ros.h>
#include <ros/callback_queue.h>
#include <std_srvs/Empty.h>
#include <std_msgs/String.h>
#include <robot_process_msgs/AliveSignal.h>
#include <robot_process_msgs/ProcessState.h>
#include <robot_process_msgs/ProcessError.h>

#define STATE_CREATED             robot_process_msgs::ProcessState::Created
#define STATE_READY_TO_START      robot_process_msgs::ProcessState::ReadyToStart
#define STATE_RUNNING             robot_process_msgs::ProcessState::Running
#define STATE_PAUSED              robot_process_msgs::ProcessState::Paused


#define STATE_STARTED             robot_process_msgs::ProcessState::Started
#define STATE_NOT_STARTED         robot_process_msgs::ProcessState::NotStarted


/*!********************************************************************************************************************
 *  \class      RobotProcess
 *  \brief      This is the core class that every ROS node has to inherit.
 *  \details    The RobotProcess base class adds the following functionalities to the derived
 *              classes that inherits it:\n
 *              - Declaration of all the states a ROS node can be at.
 *              - Creation of a signal sending thread: By deriving this class the node will
 *                  create a thread with the only purpose of sending its state to a PerformanceMonitor,
 *                  that will be hearing at the 'State' topic.
 *              - Declaration of methods that the derived class will have to implement in order to
 *                  add the desired functionality to the ROS node.
 * 
 *********************************************************************************************************************/
class RobotProcess
{
//variables
public:
  //! States match the values defined in ProcessState.msg
  using State = uint8_t;

  //! This enum defines all posible RobotProcess errors that can be sent to the PerformanceMonitor.
  typedef enum
  {
    UnexpectedProcessStop,
    InvalidInputData,
    SafeguardRecoverableError,
    SafeguardFatalError,
  } Error;

private:

  pthread_t t1;                     //!< Thread handler.

  // Node handle
protected:
  ros::NodeHandle* node_handle_;
  ros::NodeHandle* node_handle_private_;

private:
  std::string watchdog_topic;       //!< Attribute storing topic name to send alive messages to the PerformanceMonitor.
  std::string error_topic;          //!< Attribute storing topic name to send errors to the PerformanceMonitor.

  ros::ServiceServer startServerSrv;  //!< ROS service handler used to order a process to start.
  ros::ServiceServer stopServerSrv;  //!< ROS service handler used to order a process to stop.

  ros::Publisher state_pub;            //!< ROS publisher handler used to send state messages.
  ros::Publisher error_pub;            //!< ROS publisher handler used to send error messages.

  robot_process_msgs::AliveSignal state_message; //!< Message of type state.

protected:
  State current_state;               //!< Attribute storing current state of the process.
  std::string robot_id;              //!< Attribute storing the robot on which is executing the process.
  std::string hostname;              //!< Attribute storing the compouter on which the process is executing.

//methods
public:
  //! Constructor.
  RobotProcess(int argc,char **argv);
      
  ~RobotProcess();
    
  //!  This function calls to ownSetUp().
  void setUp();

  //!  This function calls to ownStart().
  //!  The first time is called this function calls to ownRun().
  void start();

  //!  This function calls to ownStop().
  void stop();

  /*!*****************************************************************************************************************
  * \brief This function calls to ownRun() when the process is Running.
  * \details This function must be called by the user in ownRun when he is implementing a synchronus execution, when using ros::spinOnce().
  * Don't use this function if using ros::spin().
  *******************************************************************************************************************/
  void run();

  // For async run
  void runAsyncMonothread();
  void runAsyncMultithread();

   /*!*****************************************************************************************************************
   * \details If the node has an already defined state (Waiting, Running...) returns
   * the state as an Integer, if not it returns -1 to indicate the current state is undefined.
   * \return Void function
   *******************************************************************************************************************/
  State getState();

  /*!******************************************************************************************************************
   * \details The function accepts one of the already defined states to modify the 'curent_state' attribute. 
   * It also sends and alive message to the PerformanceMonitor indicating the new state of the node.
   * \param   new_state The new state the process is going to have.
   * \return  The current state of the process.
   *******************************************************************************************************************/
  void setState(State new_state);

  /*!******************************************************************************************************************
   * \brief Send a RobotProcess.error to the Processs Monitor
   * \details This function is a first aproach at error handling. The error comunication between nodes
   * is performed by a two-part message. The first part indicates the type of message we are sending 
   * (info, warning, error) while the second part offers a detailed description of the problem.
   * \param [in] type            The type of error we are going to send.
   * \param [in] reference_code  This is a numeric code that may be usefull during error processing.
   * \param [in] location        The location is a string that explains at which function the error occured.
   * \param [in] description     Another String for the human reader that explains in detail the error.
   *******************************************************************************************************************/
  void notifyError(Error type, int reference_code, std::string location, std::string description);

  /*!******************************************************************************************************************
   * \brief The stateToString method transforms the recieved state into a human readable String.
   * \param [in] state The received state that need to be transformed.
   * \return the state in a String form.
   *******************************************************************************************************************/
  std::string stateToString(State state);



private:

  //!  This function sends an alive message to the PerformanceMonitor indicating the current node state.
  void notifyState();

  /*!******************************************************************************************************************
   * \brief This function sends an alive message to the PerformanceMonitor indicating the current node state.
   * \param [in] state State that has to be sent to the PerformanceMonitor.
   *******************************************************************************************************************/
  void notifyState(State state);

  /*!******************************************************************************************************************
   * \brief This function has the purpose to serve as the thread execution point.
   * \param [in] argument Function which has to be executed by the thread.
   *******************************************************************************************************************/
  static void * threadRun(void * argument);

  //!  This function implements the thread's logic.
  void threadAlgorithm();
  
  /*!******************************************************************************************************************
   * \brief This ROS service set RobotProcess in READY_TO_START state and calls function stop.
   * \details Currently, this service should only be called if the process is running. In the future
   * it will also be correct to call this service when the process is paused.
   * \param [in] request 
   * \param [in] response 
   *******************************************************************************************************************/ 
  bool stopServCall(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response);
  
  /*!******************************************************************************************************************
   * \brief This ROS service set RobotProcess in RUNNING state and calls function start.
   * \details Currently, this service should only be called if the process is ready to start. In the future
   * it will also be correct to call this service when the process is paused or running.
   * \param [in] request 
   * \param [in] response 
   *******************************************************************************************************************/ 
  bool startServCall(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response);

protected:
  /*!******************************************************************************************************************
   * \details All functions starting with 'own' has to be implemented at the derived class.
   * This function is executed in setUp() and it's purpose is to set up all the parameters.
   * the developer considers necesary when needed.
   *******************************************************************************************************************/
  virtual void ownSetUp()= 0;

  /*!******************************************************************************************************************
   * \details All functions starting with 'own' has to be implemented at the derived class.
   * This function is executed in start(), and it's purpose is to connect to all topics and services. 
   * Publisher, Subscriber, ServiceServer and ServiceClient definitions should be implemented here. 
   *******************************************************************************************************************/
  virtual void ownStart()=0;

  /*!******************************************************************************************************************
   * \details All functions starting with 'own' has to be implemented at the derived class.
   * This function is executed in stop(), and it's purpose is to shutdown all topics and services.
   * Shutdown must be called for every Publisher, Subscriber, ServiceServer and ServiceClient defined in ownStart 
   *******************************************************************************************************************/
  virtual void ownStop()=0;

  /*!******************************************************************************************************************
   * \details All functions starting with 'own' has to be implemented at the derived class.
   * This function is executed in run(), only if the process is Running.
   * The user should define this function only when implementing a synchronus execution
   *******************************************************************************************************************/
  virtual void ownRun()=0;

};
#endif
