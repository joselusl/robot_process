/*!*******************************************************************************************
 *  \file       robot_process.cpp
 *  \brief      RobotProcess implementation file.
 *  \details    This file implements the RobotProcess class.
 *  \authors    Jose Luis Sanchez-Lopez
 *
 *     This program is free software: you can redistribute it and/or modify 
 *     it under the terms of the GNU General Public License as published by 
 *     the Free Software Foundation, either version 3 of the License, or 
 *     (at your option) any later version. 
 *   
 *     This program is distributed in the hope that it will be useful, 
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 *     GNU General Public License for more details. 
 *   
 *     You should have received a copy of the GNU General Public License 
 *     along with this program. If not, see http://www.gnu.org/licenses/. 
 ********************************************************************************/

#include "robot_process/robot_process.h"

RobotProcess::RobotProcess(int argc, char **argv)
{
    //Ros Init
    ros::init(argc, argv, ros::this_node::getName());

    // Node handle
    node_handle_=new ros::NodeHandle();
    node_handle_private_=new ros::NodeHandle("~");

  watchdog_topic = "process_alive_signal";
  error_topic = "process_error";
  char buf[32];  
  gethostname(buf,sizeof buf);  
  hostname.append(buf);

  current_state = STATE_CREATED; //This State is not going to be sent. It will we significant in the future when we implement state checks.

  return;
}

RobotProcess::~RobotProcess()
{
  pthread_cancel(t1);
  if(node_handle_)
      delete node_handle_;
  if(node_handle_private_)
      delete node_handle_private_;

  return;
}

void RobotProcess::setUp()
{
  state_pub = node_handle_->advertise<robot_process_msgs::AliveSignal>(watchdog_topic, 10);
  error_pub = node_handle_->advertise<robot_process_msgs::ProcessError>(error_topic, 10);
  
  stopServerSrv=node_handle_->advertiseService(ros::this_node::getName()+"/stop",&RobotProcess::stopServCall,this);
  startServerSrv=node_handle_->advertiseService(ros::this_node::getName()+"/start",&RobotProcess::startServCall,this);

  notifyState(); //First state nNotification, the current state is STATE_CREATED

  pthread_create( &t1, NULL, &RobotProcess::threadRun,this);
  ownSetUp();
  setState(STATE_READY_TO_START);
}

void RobotProcess::start()
{
  setState(STATE_RUNNING);
  ownStart();
}

void RobotProcess::stop()
{
  setState(STATE_READY_TO_START);
  ownStop();
}

std::string RobotProcess::stateToString(State state)
{
  std::string result;
  switch(state)
  {
    case STATE_CREATED:
      result="Created";
      break;
    case STATE_READY_TO_START:
      result="ReadyToStart";
      break;
    case STATE_RUNNING:
      result="Running";
      break;
    case STATE_PAUSED:
      result="Paused";
      break;
    case STATE_STARTED:
      result="Started";
      break;
    case STATE_NOT_STARTED:
      result="NotStarted";
      break;
    default:
      ROS_WARN("In node %s, method stateToString received a invalid State. Value received is %d.",ros::this_node::getName().c_str(),state);
      break;
  }

  return result;
}

RobotProcess::State RobotProcess::getState()
{
  return current_state;
}

void RobotProcess::setState(State new_state)
{
  if (new_state==STATE_CREATED || new_state==STATE_READY_TO_START || new_state==STATE_RUNNING || new_state==STATE_PAUSED \
        || new_state==STATE_STARTED || new_state==STATE_NOT_STARTED)
  {
    current_state = new_state;
    notifyState();
  }
  else
  {
    ROS_ERROR("In node %s, current state cannot be changed to new state %d",ros::this_node::getName().c_str(),new_state);
  }
}

void RobotProcess::notifyState()
{
  State _current_state = getState();
  if (_current_state==STATE_CREATED || _current_state==STATE_READY_TO_START || _current_state==STATE_RUNNING || _current_state==STATE_PAUSED \
        || _current_state==STATE_STARTED || _current_state==STATE_NOT_STARTED)
  {
    state_message.header.stamp = ros::Time::now();
    state_message.hostname = hostname;
    state_message.process_name = ros::this_node::getName();
    state_message.current_state.state = _current_state;
    state_pub.publish(state_message);
  }
  else
  {
    ROS_ERROR("In node %s, current state is invalid, therefore it is not sent",ros::this_node::getName().c_str());
  }
}

void RobotProcess::notifyState(State current_state)
{
  state_message.header.stamp = ros::Time::now();
  state_message.process_name = ros::this_node::getName();
  state_message.hostname = hostname;
  state_message.current_state.state = (int)current_state;
  state_pub.publish(state_message);
}

void RobotProcess::notifyError(Error type, int reference_code, std::string function, std::string description)
{
  robot_process_msgs::ProcessError error_message;
  error_message.header.stamp = ros::Time::now();
  error_message.error_type.value = (int)type;
  error_message.hostname = hostname;
  error_message.process_name = ros::this_node::getName();
  error_message.function = function;
  error_message.description = description;
  error_message.reference_code = reference_code;
  error_pub.publish(error_message);
}

// COMMON METHODS
void * RobotProcess::threadRun(void * argument)
{
  ((RobotProcess *) argument)->threadAlgorithm();
  return NULL;
}

void RobotProcess::threadAlgorithm()
{
  ros::Rate r(1);
  while(ros::ok())
  {
    notifyState();
    r.sleep();
  }
}

bool RobotProcess::stopServCall(std_srvs::Empty::Request &request, std_srvs::Empty::Response &response)
{
  if(current_state==STATE_RUNNING)
  {
    stop();
    return true;
  }
  else
  {
    ROS_WARN("Node %s received a stop call when it was already stopped",ros::this_node::getName().c_str());
    return false;
  }
}

bool RobotProcess::startServCall(std_srvs::Empty::Request &request, std_srvs::Empty::Response &response)
{
  if(current_state==STATE_READY_TO_START)
  {
    start();
    return true;
  }
  else
  {
    ROS_WARN("Node %s received a start call when it was already running",ros::this_node::getName().c_str());
    return false;
  }
}

void RobotProcess::run()
{
  if(current_state==STATE_RUNNING)
    ownRun();
}

void RobotProcess::runAsyncMonothread()
{
    // Spin
    ros::spin();
}

void RobotProcess::runAsyncMultithread()
{
    // Spin
    ros::MultiThreadedSpinner spinner(4); // Use 4 threads
    spinner.spin(); // spin() will not return until the node has been shutdown
}
